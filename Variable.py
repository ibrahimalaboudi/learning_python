counter = 100
miles   = 1000.0
name    = "ibrahim"
print counter
print miles
print  name
print "----------------------------"
a = 1
b = 2
c = 3
d = "ibrahim123"
print a , b , c , d
print "----------------------------"
str = 'hello world'
print str
print str[0]
print str[2:5]
print  str[2:]
print  str*2
print  str  + "TEST"
print "----------------------------"
list = ['abcd', 786 , 2.23 ,'jhon', 70.2]
tinylist = [123, 'jhon']
print  list
print  list[0]
print  list [1:3]
print  list [2:]
print  tinylist*2
print  list + tinylist
print "----------------------------"
tuple = ('abcd', 786, 2.23, 'ibrah', 70.2)
tinytuble = (123, "ibrahim")
print tuple
print tuple[0]
print tuple[1:3]
print tuple[2:]
print  tinytuble* 2
print  tuple + tinytuble
print "----------------------------"
dict = {}
dict['one'] = "this is one"
dict [2] = "this is two"
tinydict = {'name': 'jhon', 'code' :6734, 'dept':'sales'}
print dict ['one']
print dict [2]
print tinydict
print  tinydict
print  tinydict.keys()
print  tinydict.values()