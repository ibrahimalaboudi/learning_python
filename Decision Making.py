#IF...ELIF...ELSE Statments
var1= 100
if var1:
    print "1 - got a true expression value"
    print var1
else:
    print "1 - got a false expression value"
    print var1
var2 = 0
if var2:
    print "2 - got a true expression value"
    print var2
else:
    print "2 - got a false expression value"
    print var2
print "Good Bye!"
print "------------------------------------------"
var= 100
if var == 200:
    print "1 - got a true expression value"
    print var
elif var == 150:
    print " 2 - got a true expression value"
    print var
elif var ==100:
    print "3- got a true expression value"
    print var
else:
    print "4 - got a false expression value"
    print var
print "GoodBye!"
print "------------------------------------------"
# Nested IF Statements
var= 100
if var < 200:
    print "Expression Value is Less than 200"
    if var == 150:
        print "which is 150"
    elif var == 100:
        print "which is 100"
    elif var == 50:
        print "which is 50"
    elif var <50:
        print "Expression value is less than 50"
else:
    print "Could not find a true expression"
print "BYE BYE !!!"