count = 0
while (count < 9):
    print 'The count is:' , count
    count = count + 1
print "Good bye!"
print "------------------------------------"
#The infinite loop
var = 1
while var == 1 :
    num = raw_input("enter a number :")
    print "you entered: " , num
print "Good Bye!"
print "------------------------------------"
counts = 0
while counts <5:
    print counts, "is less then 5"
    counts = counts +1
else:
    print counts, "is not less then 5"
print "------------------------------------"
# for loop
for letter in 'IBRAHIM':
    print  'Current Letter :', letter
fruits = ['banana', 'apple', 'mango']
for fruits in fruits:
    print 'Current fruits :', fruits
print "Good bye !"
print "------------------------------------"
for n in range(10,20):
    for i in range(2,n):
        if n%i ==0:
            j=n/i
            print '%d equals %d * %d' % (n, i, j)
            break
        else:
            print  n, 'is a prime number'
print "------------------------------------"